package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.Request;
import static akka.pattern.Patterns.gracefulStop;
import akka.pattern.AskTimeoutException;

import java.time.Duration;
import java.util.concurrent.*;

import java.util.concurrent.CompletionStage;

/**
 * @author Remi SHARROCK and Axel Mathieu
 * @description Create an actor and passing his reference to
 *				another actor by message.
 */
public class StoppingActors {

	public static void main(String[] args) {
		// Instantiate an actor system
		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef a1 = system.actorOf(MyActor.createActor(), "a1");
	    final ActorRef a2 = system.actorOf(MyActor.createActor(), "a2");
		final ActorRef a3 = system.actorOf(MyActor.createActor(), "a3");
		final ActorRef a4 = system.actorOf(MyActor.createActor(), "a4");
			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
	    a1.tell(new Request("Hello1", a2), a2);
		a1.tell(new Request("Hello2", a2), a2);
		a1.tell(new Request("Hello1", a2), a2);
		a1.tell(new Request("Hello2", a2), a2);
		a1.tell(new Request("stop", a2), a2);
		a1.tell(new Request("Hello2", a2), a2);
		a2.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());
		a3.tell(new Request("Hello1", a4), a4);
		a3.tell(new Request("Hello2", a4), a4);
		a3.tell(new Request("Hello1", a4), a4);
		a3.tell(new Request("Hello2", a4), a4);
		a3.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());
		a3.tell(new Request("Hello1", a4), a4);
		try {
			CompletionStage<Boolean> stopped =
					gracefulStop(a4, Duration.ofSeconds(1));
			stopped.toCompletableFuture().get(1, TimeUnit.SECONDS);
			// the actor has been stopped
		} catch (AskTimeoutException e) {
			// the actor wasn't stopped within 5 seconds
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		} catch (TimeoutException e) {
			e.printStackTrace();
		}
		a3.tell(new Request("Hello2", a4), a4);
	
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
	}

}
