package demo;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedAbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class MyActor extends UntypedAbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	// Actor reference
	private ActorRef actorRef;

	public MyActor() {}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(MyActor.class, () -> {
			return new MyActor();
		});
	}

	public static class Request {
		public final String query;
		public final ActorRef replyTo;

		public Request(String query, ActorRef replyTo) {
			this.query = query;
			this.replyTo = replyTo;
		}
	}

	public static class Response {
		public final String result;

		public Response(String result) {
			this.result = result;
		}
	}
	@Override
	public void onReceive(Object message) throws Throwable {
		if(message instanceof Request){
		log.info("["+getSelf().path().name()+"] received message " + ((Request) message).query + " from ["+ getSender().path().name() +"]");
		((Request) message).replyTo.tell(new Response("Hi"), getSelf());
		if(((Request) message).query.equals("stop"))
		{
			getContext().stop(getSelf());
		}
		}
		if(message instanceof Response){
			log.info("["+getSelf().path().name()+"] received message " + ((Response)message).result+" from ["+ getSender().path().name() +"]");
		}
	}


	/**
	 * alternative for AbstractActor
	 * @Override
	public Receive createReceive() {
		return receiveBuilder()
				// When receiving a new message containing a reference to an actor,
				// Actor updates his reference (attribute).
				.match(ActorRef.class, ref -> {
					this.actorRef = ref;
					log.info("Actor reference updated ! New reference is: {}", this.actorRef);
				})
				.build();
	}
	 */
}
