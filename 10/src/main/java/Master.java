import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;

import java.util.ArrayList;
import java.util.List;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class Master extends AbstractActor {
    Router router;
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

        List<Routee> routees = new ArrayList<Routee>();




    public static Props createActor() {
        return Props.create(Master.class, () -> {
            return new Master();
        });
    }
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if("join" == m.data)
        {
            ActorRef r = m.ac;
            routees.add(new ActorRefRoutee(r));
        }
        else
        {
            router = new Router(new RoundRobinRoutingLogic(),routees);
            for(int i = 0; i < router.routees().size(); i++)
                router.route(m,getSender());
        }
    }
}
