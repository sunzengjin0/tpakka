package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;

import java.util.ArrayList;

import static java.lang.System.exit;

/**
 * @author Remi SHARROCK
 * @description
 */
public class Multicast {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef sender = system.actorOf(MyActor.createActor(), "sender");
		final ActorRef r1 = system.actorOf(MyActor.createActor(), "r1");
		final ActorRef r2 = system.actorOf(MyActor.createActor(), "r2");
		final ActorRef r3 = system.actorOf(MyActor.createActor(), "r3");
		final ActorRef multi = system.actorOf(Multicaster.createActor(r1), "multi");

		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		MyMessage m1 = new MyMessage("Group1",new ArrayList<ActorRef>(){{add(r1); add(r2);}});
		MyMessage m2 = new MyMessage("Group2",new ArrayList<ActorRef>(){{add(r2); add(r3);}});
		MyMessage m3 = new MyMessage("SendGroup1Hello");
		MyMessage m4 = new MyMessage("SendGroup2World");
		multi.tell(m1,sender);
		multi.tell(m2, sender);
		multi.tell(m3, sender);
		multi.tell(m4, sender);
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
		try {
			waitBeforeTerminate(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		exit(0);
	}


	public static void waitBeforeTerminate(int a) throws InterruptedException {
		Thread.sleep(a);
	}

}
