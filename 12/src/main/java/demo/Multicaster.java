package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;
public class Multicaster extends AbstractActor{
    private ActorRef actorRef;
    Map<String, ArrayList<ActorRef>> groups = new HashMap<String, ArrayList<ActorRef>>();
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    Multicaster(ActorRef a){
        this.actorRef = a;
    }
    // Static function creating actor
    public static Props createActor(ActorRef a) {
        return Props.create(Multicaster.class, () -> {
            return new Multicaster(a);
        });
    }
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        //log.info();
        if(m.data.substring(0, 5).equals("Group")) {
            log.info("group");
            groups.put(m.data,m.ac);
        }
        else if(m.data.substring(0,4).equals("Send"))
        {
            log.info("send");
            ArrayList<ActorRef> temp = groups.get(m.data.substring(4, 10));
            for(int i = 0; i< temp.size(); i++)
            {
                temp.get(i).tell(new MyActor.MyMessage(m.data.substring(10)),getSelf());
            }
        }
    }

}
