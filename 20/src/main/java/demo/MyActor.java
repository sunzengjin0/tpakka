package demo;

import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.actor.ActorRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyActor extends AbstractActor {

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	ArrayList<ActorRef> reachable = new ArrayList<ActorRef>();
	Map<Integer, Boolean> seq = new HashMap<Integer, Boolean>();
	public MyActor() {
	}
	private ActorRef session;
	// Static function creating actor
	public static Props createActor() {
		return Props.create(MyActor.class, () -> {
			return new MyActor();
		});
	}

	static public class MyMessage {
		public final String data;
		public  ActorRef ac;
		public int sequence;
		public MyMessage(String data, ActorRef a ) {
			this.data = data;
			this.ac = a;
			this.sequence = sequence;
		}
		public MyMessage(String data, int sequence) {
			this.data = data;
			this.sequence = sequence;
		}
		public MyMessage(String data) {
			this.data = data;
			this.sequence = 1000;
		}
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
			.match(MyMessage.class, this::receiveFunction)
			.build();
	  }

	  public void receiveFunction(MyMessage m) throws InterruptedException {
		log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]" + "] with sender: [" +
				getSender().path().name() + "]");
		if(m.data.equals("Reach"))
		{
			reachable.add(m.ac);
		}
		else if(m.data.equals("Display"))
		{
			String temp = getSelf().path().name();
			for(int i = 0; i < reachable.size(); i++)
				temp = temp + " " + reachable.get(i).path().name();
			log.info(temp);
		}
		else
		{
				for (int i = 0; i < reachable.size(); i++)
					reachable.get(i).tell(m, getSelf());
		}
	  }


}
