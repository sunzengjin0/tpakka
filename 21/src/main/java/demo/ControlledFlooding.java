package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;
import scala.math.Integral;

import java.util.ArrayList;
import java.util.Vector;

import static java.lang.System.exit;

/**
 * @author Remi SHARROCK
 * @description
 */
public class ControlledFlooding {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		int[][] matrix  =
				{
						{0, 1, 1, 0, 0},
						{0, 0, 0, 1, 0},
						{0, 0, 0, 1, 0},
						{0, 0, 0, 0, 1},
						{0, 1, 0, 0, 0}
				};
		ArrayList<ActorRef> members = new ArrayList<>();
		// Instantiate first and second actor
		for(int i = 0; i < matrix[0].length; i++)
		{
			members.add(system.actorOf(MyActor.createActor(), "actor" + (i + 1)));
		}

		for(int i = 0; i < matrix[0].length; i++)
		{
			for(int j = 0; j < matrix[0].length; j++)
			{
				if(matrix[i][j] == 1)
				{
					members.get(i).tell(new MyMessage("Reach", members.get(j)), ActorRef.noSender());
				}
			}
		}
		members.get(0).tell(new MyMessage("Hello", 1), ActorRef.noSender());

		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1,

	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
		try {
			waitBeforeTerminate(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		exit(0);
	}


	public static void waitBeforeTerminate(int a) throws InterruptedException {
		Thread.sleep(a);
	}

}
