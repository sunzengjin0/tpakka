

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

/**
 * @author Remi SHARROCK
 * @description
 */
public class LoadBalancerRoundRobin {

    public static void main(String[] args) {

        final ActorSystem system = ActorSystem.create("system");

        // Instantiate first and second actor
        final ActorRef a = system.actorOf(MyActor.createActor(), "a");
        final ActorRef b = system.actorOf(MyActor.createActor(), "b");
        final ActorRef m = system.actorOf(MyRouter.createActor(), "m");
        final ActorRef c = system.actorOf(MyActor.createActor(), "c");
        // send to a1 the reference of a2 by message
        //be carefull, here it is the main() function that sends a message to a1,
        //not a1 telling to a2 as you might think when looking at this line:
        MyActor.MyMessage ma = new MyActor.MyMessage("join");
        MyActor.MyMessage mb = new MyActor.MyMessage("unjoin");
        MyActor.MyMessage md = new MyActor.MyMessage("Hello", a);
        m.tell(ma,b);
        m.tell(ma,c);
        m.tell(md,a);
        m.tell(md,a);
        m.tell(mb,c);
        m.tell(md,a);
        m.tell(md,a);

        // We wait 5 seconds before ending system (by default)
        // But this is not the best solution.
        try {
            waitBeforeTerminate();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            system.terminate();
        }
    }

    public static void waitBeforeTerminate() throws InterruptedException {
        Thread.sleep(5000);
    }

}
