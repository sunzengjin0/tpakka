import akka.actor.AbstractActor;
import akka.actor.Props;
import akka.routing.*;

import java.util.ArrayList;
import java.util.List;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class MyRouter extends AbstractActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    List<Routee> routees = new ArrayList<Routee>();
    Router router  = new Router(new RoundRobinRoutingLogic(), routees);


    public static Props createActor() {
        return Props.create(MyRouter.class, () -> {
            return new MyRouter();
        });
    }
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if("join".equals(m.data))
        {
            routees.add(new ActorRefRoutee(getSender()));
            //router1.tell(new akka.routing.AddRoutee(new ActorRefRoutee(getSender())),ActorRef.noSender());
            router  = new Router(new RoundRobinRoutingLogic(), routees);
        }
        else if(m.data.equals("unjoin"))
        {
            //router.removeRoutee(new ActorRefRoutee(getSender()));
            routees.remove(new ActorRefRoutee(getSender()));
            //router1.tell(new akka.routing.RemoveRoutee(new ActorRefRoutee(getSender())),ActorRef.noSender());
            router  = new Router(new RoundRobinRoutingLogic(), routees);

        }
        else
        {

            router.route(m, getSelf());
        }
    }
}
