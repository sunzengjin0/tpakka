import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.actor.ActorRef;

public class MyActor extends AbstractActor {

    // Logger attached to actor
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);

    public MyActor() {
    }

    // Static function creating actor
    public static Props createActor() {
        return Props.create(MyActor.class, () -> {
            return new MyActor();
        });
    }

    static public class MyMessage {
        public final String data;
        public final ActorRef ac;
        public MyMessage(String data, ActorRef ac) {
            this.data = data;
            this.ac = ac;
        }
        public MyMessage(String data) {
            this.data = data;
            this.ac = null;
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyMessage.class, this::receiveFunction)
                .build();
    }

    public void receiveFunction(MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
    }


}
