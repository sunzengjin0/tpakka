package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;

import static java.lang.System.exit;

/**
 * @author Remi SHARROCK
 * @description
 */
public class SessionChildActor {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
		final ActorRef a = system.actorOf(MyActor.createActor(), "a");
		final ActorRef create = system.actorOf(SessionManager.createActor(a), "create");

		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		MyMessage m1 = new MyMessage("CreateSession");
		MyMessage m2 = new MyMessage("EndSession");
		create.tell(m1, a);
		create.tell(m2, a);

	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
		try {
			waitBeforeTerminate(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		exit(0);
	}


	public static void waitBeforeTerminate(int a) throws InterruptedException {
		Thread.sleep(a);
	}

}
