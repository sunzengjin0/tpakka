package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;
public class SessionManager extends AbstractActor{
    private ActorRef actorRef;
    private ActorRef session;
    ArrayList<ActorRef> members = new  ArrayList<ActorRef>();
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    SessionManager(ActorRef a){
        this.actorRef = a;
    }
    // Static function creating actor
    public static Props createActor(ActorRef a) {
        return Props.create(SessionManager.class, () -> {
            return new SessionManager(a);
        });
    }
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if(m.data.equals("CreateSession"))
        {
            session = getContext().actorOf(MyActor.createActor(), "session");
            getSender().tell(new MyActor.MyMessage("Created", session), getSelf());
        }
        else if(m.data.equals("EndSession"))
        {
            session.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());
        }
    }

}
