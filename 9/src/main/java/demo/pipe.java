package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;
public class pipe extends AbstractActor{
    private ActorRef actorRef;
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    pipe(ActorRef a){
        this.actorRef = a;
    }
    // Static function creating actor
    public static Props createActor(ActorRef a) {
        return Props.create(pipe.class, () -> {
            return new pipe(a);
        });
    }
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if(m.data == "request")
            m.ac.tell(new MyActor.MyMessage("response", getSender()), getSelf());
    }
}
