package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;

/**
 * @author Remi SHARROCK
 * @description
 */
public class Convergecast {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef a = system.actorOf(MyActor.createActor(), "a");
		final ActorRef b = system.actorOf(MyActor.createActor(), "b");
		final ActorRef c = system.actorOf(MyActor.createActor(), "c");
		final ActorRef d = system.actorOf(MyActor.createActor(), "d");
		final ActorRef t = system.actorOf(Merger.createActor(d), "merger");

		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
			MyMessage m = new MyMessage("join", b);
	    t.tell(m, a);
		t.tell(m, b);
		t.tell(m, c);
		m = new MyMessage("Hello", b);
		t.tell(m, a);
		t.tell(m, b);
		t.tell(m, c);
		try {
			waitBeforeTerminate(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
		m = new MyMessage("unjoin", b);
		t.tell(m, c);
		m = new MyMessage("Hello2", b);
		t.tell(m, a);
		t.tell(m, b);
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	}

	public static void waitBeforeTerminate(int a) throws InterruptedException {
		Thread.sleep(a);
	}

}
