package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;
public class Merger extends AbstractActor{
    private ActorRef actorRef;
    ArrayList<ActorRef> members = new ArrayList<ActorRef>();
    Map<String, Integer> active = new HashMap<String, Integer>();
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    Merger(ActorRef a){
        this.actorRef = a;
    }
    // Static function creating actor
    public static Props createActor(ActorRef a) {
        return Props.create(Merger.class, () -> {
            return new Merger(a);
        });
    }
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if(m.data == "join") {
            members.add(getSender());

        }
        else if(m.data == "unjoin")
        {
            members.remove(getSender());
        }
        else
        {
            if(!active.containsKey(m.data))
            {
                active.put(m.data, 1);
            }
            else
            {
                active.replace(m.data, active.get(m.data) + 1);
            }
            if(active.get(m.data) == members.size())
            {
                actorRef.tell(new MyActor.MyMessage(m.data, members),getSelf());
                active.clear();
            }
        }
    }
}
