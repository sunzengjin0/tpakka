package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;

import static java.lang.System.exit;

/**
 * @author Remi SHARROCK
 * @description
 */
public class PublishSubscribe {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
		final ActorRef a = system.actorOf(MyActor.createActor(), "a");
		final ActorRef b = system.actorOf(MyActor.createActor(), "b");
		final ActorRef c = system.actorOf(MyActor.createActor(), "c");
		final ActorRef publish1 = system.actorOf(MyActor.createActor(), "publish1");
		final ActorRef publish2 = system.actorOf(MyActor.createActor(), "publish2");
		final ActorRef topic1 = system.actorOf(Topic.createActor(publish1), "topic1");
		final ActorRef topic2 = system.actorOf(Topic.createActor(publish2), "topic2");

		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		MyMessage m1 = new MyMessage("join");
		MyMessage m2 = new MyMessage("Hello");
		MyMessage m3 = new MyMessage("world");
		MyMessage m4 = new MyMessage("unjoin");
		MyMessage m5 = new MyMessage("Hello2");
		topic1.tell(m1, a);
		topic1.tell(m1, b);
		topic2.tell(m1, b);
		topic2.tell(m1, c);
		topic1.tell(m2, publish1);
		topic2.tell(m3, publish2);
		topic1.tell(m4, a);
		topic1.tell(m5, publish1);
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
		try {
			waitBeforeTerminate(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		exit(0);
	}


	public static void waitBeforeTerminate(int a) throws InterruptedException {
		Thread.sleep(a);
	}

}
