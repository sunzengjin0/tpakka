package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;
public class Topic extends AbstractActor{
    private ActorRef actorRef;
    ArrayList<ActorRef> members = new  ArrayList<ActorRef>();
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    Topic(ActorRef a){
        this.actorRef = a;
    }
    // Static function creating actor
    public static Props createActor(ActorRef a) {
        return Props.create(Topic.class, () -> {
            return new Topic(a);
        });
    }
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if(m.data.equals("join"))
        {
            members.add(getSender());
        }
        else if(m.data.equals("unjoin"))
        {
            members.remove(getSender());
        }
        else if(getSender().equals(actorRef))
        {
            for(int i = 0; i < members.size(); i ++)
            {
                members.get(i).tell(m, getSelf());
            }
        }
    }

}
