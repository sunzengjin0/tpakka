package demo;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.dispatch.Futures;
import akka.event.Logging;
import akka.event.LoggingAdapter;

public class MyActor extends AbstractActor{

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	// Actor reference
	private ActorRef actorRef;

	MyActor() {
		Futures.future(() -> "hello", getContext().dispatcher());
	}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(MyActor.class, () -> {
			return new MyActor();
		});
	}

	public static class Request {
		public final String query;
		public final ActorRef replyTo;

		public Request(String query, ActorRef replyTo) {
			this.query = query;
			this.replyTo = replyTo;
		}
	}

	public static class Response {
		public final String result;

		public Response(String result) {
			this.result = result;
		}
	}
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(String.class, this::receiveMsg1)
				.build();
	}
	private void receiveMsg1(String msg) {
		log.info(getSelf().path().name() + " " + "received " + msg + " " + "from " + getSender().path().name());
		getSender().tell(msg, getSelf());
	}


	/**
	 * alternative for AbstractActor
	 * @Override
	public Receive createReceive() {
		return receiveBuilder()
				// When receiving a new message containing a reference to an actor,
				// Actor updates his reference (attribute).
				.match(ActorRef.class, ref -> {
					this.actorRef = ref;
					log.info("Actor reference updated ! New reference is: {}", this.actorRef);
				})
				.build();
	}
	 */
}
