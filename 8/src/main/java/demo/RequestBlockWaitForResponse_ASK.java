package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import scala.concurrent.Future;
import scala.concurrent.Await;
import akka.util.Timeout;

import java.time.Duration;

import static akka.pattern.Patterns.ask;
/**
 * @author Remi SHARROCK and Axel Mathieu
 * @description Create an actor and passing his reference to
 *				another actor by message.
 */
public class RequestBlockWaitForResponse_ASK {

	public static void main(String[] args) throws Exception {
		// Instantiate an actor system
		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef a1 = system.actorOf(MyActor.createActor(), "a1");
	    final ActorRef a2 = system.actorOf(MyActor.createActor(), "a2");
	    
			// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		Timeout timeout = Timeout.create(Duration.ofSeconds(5));
		Future<Object> future1 = ask(a1, "msg", timeout);
		String result1 = (String) Await.result(future1, timeout.duration());
		Future<Object> future2 = ask(a2, "msg", timeout);
		String result2 = (String) Await.result(future2, timeout.duration());
		System.out.println(result1 + result2);
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
	}

}
