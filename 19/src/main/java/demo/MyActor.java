package demo;

import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.actor.ActorRef;

import java.util.ArrayList;
import java.util.List;

public class MyActor extends AbstractActor {

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	ArrayList<ActorRef> reachable = new ArrayList<ActorRef>();
	public MyActor() {
	}
	private ActorRef session;
	// Static function creating actor
	public static Props createActor() {
		return Props.create(MyActor.class, () -> {
			return new MyActor();
		});
	}

	static public class MyMessage {
		public final String data;
		public  ActorRef ac;
		public MyMessage(String data, ActorRef a ) {
			this.data = data;
			this.ac = a;
		}
		public MyMessage(String data) {
			this.data = data;
		}
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
			.match(MyMessage.class, this::receiveFunction)
			.build();
	  }

	  public void receiveFunction(MyMessage m) throws InterruptedException {
		log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]" + "] with sender: [" +
				getSender().path().name() + "]");
		if(m.data.equals("Reach"))
		{
			reachable.add(m.ac);
		}
		else if(m.data.equals("Display"))
		{
			String temp = getSelf().path().name() + ":";
			for(int i = 0; i < reachable.size(); i++)
				temp = temp + " " + reachable.get(i).path().name();
			log.info(temp);
		}
	  }


}
