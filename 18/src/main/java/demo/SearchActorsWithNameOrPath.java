package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;

/**
 * @author Remi SHARROCK
 * @description
 */
public class SearchActorsWithNameOrPath {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
	    final ActorRef a = system.actorOf(MyActor.createActor(), "a");
		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
		a.tell(new MyMessage("CREATE"), ActorRef.noSender());
		a.tell(new MyMessage("CREATE"), ActorRef.noSender());
		a.tell(new MyMessage("SEARCH"), ActorRef.noSender());
	    try {
			waitBeforeTerminate();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			system.terminate();
		}
	}

	public static void waitBeforeTerminate() throws InterruptedException {
		Thread.sleep(5000);
	}

}
