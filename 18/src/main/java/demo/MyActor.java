package demo;

import akka.actor.ActorSelection;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.actor.ActorRef;

import java.util.ArrayList;
import akka.actor.ActorIdentity;
import akka.actor.ActorSelection;
import akka.actor.Identify;

public class MyActor extends AbstractActor {

	// Logger attached to actor
	private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
	ArrayList<ActorRef> members = new ArrayList<ActorRef>();
	int i = 1 ;
	public MyActor() {
	}

	// Static function creating actor
	public static Props createActor() {
		return Props.create(MyActor.class, () -> {
			return new MyActor();
		});
	}

	static public class MyMessage {
		public final String data;
		public final ActorRef ac;
		public MyMessage(String data, ActorRef ac) {
			this.data = data;
			this.ac = ac;
		}
		public MyMessage(String data) {
			this.data = data;
			this.ac = null;
		}
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
			.match(MyMessage.class, this::receiveFunction)
				.match(ActorIdentity.class,this::getactor)
			.build()

				;
	  }

	  public void receiveFunction(MyMessage m){
		log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
		if(m.data.equals("CREATE"))
		{
			ActorRef temp = getContext().actorOf(MyActor.createActor(), "actor"+i);
			i++;
		}
		else if(m.data.equals("SEARCH"))
		{
			ActorSelection t1= getContext().actorSelection("../a");
			ActorSelection t2= getContext().actorSelection("../a/*");
			ActorSelection t3= getContext().actorSelection("/user/*");
			ActorSelection t4= getContext().actorSelection("/system/*");
			ActorSelection t5= getContext().actorSelection("/deadLetters/*");
			ActorSelection t6= getContext().actorSelection("/temp/*");
			ActorSelection t7= getContext().actorSelection("/remote/*");
			t1.tell(new Identify(1), getSelf());
			t2.tell(new Identify(1), getSelf());
			t3.tell(new Identify(1), getSelf());
			t4.tell(new Identify(1), getSelf());
			t5.tell(new Identify(1), getSelf());
			t6.tell(new Identify(1), getSelf());
			t7.tell(new Identify(1), getSelf());
		}
	  }
	public void getactor(ActorIdentity a)
	{
		if(!(a.getRef() == null))
			log.info(a.getRef().path().name());
		else
		{
			log.info("nothing");
		}
	}

}
