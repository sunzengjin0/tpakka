package demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import demo.MyActor.MyMessage;

import static java.lang.System.exit;

/**
 * @author Remi SHARROCK
 * @description
 */
public class ElasticLoadBalancer {

	public static void main(String[] args) {

		final ActorSystem system = ActorSystem.create("system");
		
		// Instantiate first and second actor
		final ActorRef a = system.actorOf(MyActor.createActor(), "a");
		final ActorRef max = system.actorOf(MyActor.createActor(), "max");
		final ActorRef balance = system.actorOf(LoadBalancer.createActor(max), "balance");

		// send to a1 the reference of a2 by message
			//be carefull, here it is the main() function that sends a message to a1, 
			//not a1 telling to a2 as you might think when looking at this line:
		MyMessage m1 = new MyMessage("max2");
		MyMessage m2 = new MyMessage("t1");
		MyMessage m3 = new MyMessage("t2");
		MyMessage m4 = new MyMessage("t3");
		balance.tell(m1,max);
		balance.tell(m2, a);
		balance.tell(m3, a);
		balance.tell(m4, a);

	    // We wait 5 seconds before ending system (by default)
	    // But this is not the best solution.
		try {
			waitBeforeTerminate(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		exit(0);
	}


	public static void waitBeforeTerminate(int a) throws InterruptedException {
		Thread.sleep(a);
	}

}
