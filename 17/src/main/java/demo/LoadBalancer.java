package demo;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.AbstractActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static akka.pattern.Patterns.ask;
import static akka.pattern.Patterns.pipe;
public class LoadBalancer extends AbstractActor{
    private ActorRef actorRef;
    private ActorRef session;
    private int max = 0;
    ArrayList<ActorRef> members = new  ArrayList<ActorRef>();
    Map<ActorRef, Integer> order = new HashMap<ActorRef, Integer>();
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    LoadBalancer(ActorRef a){
        this.actorRef = a;
    }
    // Static function creating actor
    public static Props createActor(ActorRef a) {
        return Props.create(LoadBalancer.class, () -> {
            return new LoadBalancer(a);
        });
    }
    public Receive createReceive() {
        return receiveBuilder()
                .match(MyActor.MyMessage.class, this::receiveFunction)
                .build();
    }
    public void receiveFunction(MyActor.MyMessage m){
        log.info("["+getSelf().path().name()+"] received message from ["+ getSender().path().name() +"] with data: ["+m.data+"]");
        if(m.data.length()> 3 && m.data.substring(0, 3).equals("max"))
        {
            max =Integer.parseInt(m.data.substring(3));
            log.info("max is " + max);
        }
        else if(m.data.length()>8 && m.data.substring(0, 8).equals("Finished"))
        {
            order.replace(getSender(), order.get(getSender()) - 1);
            if(order.get(getSender()) == 0)
            {
                getSender().tell(akka.actor.PoisonPill.getInstance(), getSelf());
                members.remove(getSender());
                log.info("remove " + getSender().path().name());
            }
        }
        else
        {
            ActorRef t;
            if(members.size()<max)
            {
                t = getContext().actorOf(MyActor.createActor());
                members.add(t);
            }
            else
            {
                int temp = 0;
                for(int i = 0; i < members.size(); i++)
                {
                    if(order.get(members.get(i)) < order.get(members.get(temp)))
                    {
                        temp = i;
                    }
                }
                t = members.get(temp);
            }
            t.tell(m,getSelf());
            if(!order.containsKey(t) || order.get(t) == 0)
            {
                order.put(t, 1);
            }
            else
            {
                order.replace(t, order.get(t) + 1);
            }
        }
    }

}
